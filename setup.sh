# Installing homebrew
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"


# Install brew bundle
brew bundle

# ruby install
rbenv install 2.1.3
rbenv rehash

# Installing ruby gems
gem install homesick
gem install gollum

rbenv rehash

# Jenv
curl -L -s get.jenv.io | bash

# Oh-my-zsh
curl -L http://install.ohmyz.sh | sh

