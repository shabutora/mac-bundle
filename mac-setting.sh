# For Dock
defaults write com.apple.dock pinning -string end
defaults write com.apple.dock mineffect suck

killall Dock

# For Finder
# See http://tukaikta.blog135.fc2.com/blog-entry-251.html
# See http://www.lifehacker.jp/2013/12/131217mac_terminal.html
defaults write com.apple.finder AppleShowAllFiles -bool true
defaults write com.apple.finder _FXShowPosixPathInTitle -bool true
defaults write com.apple.finder QLEnableTextSelection -bool true
defaults write com.apple.finder QLHidePanelOnDeactivate -bool true
defaults write com.apple.finder QLEnableTextSelection -bool true 
defaults write -g NSNavPanelExpandedStateForSaveMode -bool true
defaults write com.apple.desktopservices DSDontWriteNetworkStores -bool true

killall Finder

# For iTunes
defaults write com.apple.dock itunes-notifications -bool TRUE 

killall Dock

# Mute alerm
sudo nvram SystemAudioVolume=%80


